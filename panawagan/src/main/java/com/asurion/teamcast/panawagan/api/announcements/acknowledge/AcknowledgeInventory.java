/**
 * 
 */
package com.asurion.teamcast.panawagan.api.announcements.acknowledge;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;

/**
 * @author alexander.basa
 *
 */
@ApplicationScoped
public class AcknowledgeInventory {
	private Map<String,Map<String,Set<String>>> inventory;

	public AcknowledgeInventory() {
		this.inventory = new HashMap<>();
	}
	
	public void put(final String announcementId, final String accountId, final String option) {
		Map<String,Set<String>> announcements = this.inventory.get(announcementId);
		if(null == announcements) {
			announcements = new HashMap<>();
		}
		Set<String> accounts = announcements.get(option);
		if(null == accounts) {
			accounts = new HashSet<>();
			announcements.put(option, accounts);
		}
		accounts.add(accountId);
		
		this.inventory.put(announcementId, announcements);
	}
	
	public Map<String,Set<String>> list(final String announcementId) {
		return this.inventory.get(announcementId);
	}
}