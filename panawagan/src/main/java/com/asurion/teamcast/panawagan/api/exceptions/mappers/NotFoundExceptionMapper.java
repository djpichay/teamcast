/**
 * 
 */
package com.asurion.teamcast.panawagan.api.exceptions.mappers;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.asurion.teamcast.panawagan.api.exceptions.NotFoundException;

/**
 * @author alexander.basa
 *
 */
@Provider
public class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {

	@Override
	public Response toResponse(NotFoundException ex) {
		ex.printStackTrace();
		return Response.status(Response.Status.NOT_FOUND).build();
	}
}