/**
 * 
 */
package com.asurion.teamcast.panawagan.api.utils;

import org.json.JSONObject;

/**
 * @author alexander.basa
 *
 */
public class JSONUtil {

	public static String toJSONString(Object obj) {
		JSONObject json = new JSONObject(obj);
		return json.toString();
	}
	
	public static byte[] toBytes(Object obj) {
		String str = toJSONString(obj);
		return str.getBytes();
	}
}