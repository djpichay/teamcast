/**
 * 
 */
package com.asurion.teamcast.panawagan.api;

import java.util.List;

import javax.ws.rs.core.MultivaluedMap;

/**
 * @author alexander.basa
 *
 */
public interface BaseService<R> {
	
	public long create(R resource);
	public R retrieve(long id);
	public boolean update(long id, R resource);
	public boolean delete(long id);
	public List<R> search(MultivaluedMap<String,String> query);
}
