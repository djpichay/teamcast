/**
 * 
 */
package com.asurion.teamcast.panawagan.api.utils;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtilsBean;

/**
 * @author alexander.basa
 *
 */
public class NullAwareBeanUtils {
	private static NullAwareBeanUtilsBean beanUtils;

	static {
		beanUtils = new NullAwareBeanUtilsBean();
	}
	
	public static void copyProperties(Object dest, Object source) {
		try {
			beanUtils.copyProperties(dest, source);
		} catch (@SuppressWarnings("unused") Exception e) {
			//ignore
		}
	}

	private static class NullAwareBeanUtilsBean extends BeanUtilsBean {
		public NullAwareBeanUtilsBean() {
			super();
		}

		@Override
		public void copyProperty(Object dest, String name, Object value)
				throws IllegalAccessException, InvocationTargetException {
			if (null == value)
				return;
			super.copyProperty(dest, name, value);
		}
	}
}
