/**
 * 
 */
package com.asurion.teamcast.panawagan.api.dao;

import java.util.List;
import java.util.Map;

/**
 * @author alexander.basa
 *
 */
public interface BaseDao<R> {
	public long create(R resource);
	public R retreieve(long id);
	public boolean update(R resource);
	public boolean delete(long id);
	public List<R> search(Map<String,String> query);
}
