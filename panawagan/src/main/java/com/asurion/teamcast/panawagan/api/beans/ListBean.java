/**
 * 
 */
package com.asurion.teamcast.panawagan.api.beans;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author alexander.basa
 *
 */
@XmlRootElement
public class ListBean {
	private Set<String> list;

	/**
	 * @return the list
	 */
	public Set<String> getList() {
		return this.list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(Set<String> list) {
		this.list = list;
	}
}
