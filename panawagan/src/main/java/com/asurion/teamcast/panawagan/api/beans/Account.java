/**
 * 
 */
package com.asurion.teamcast.panawagan.api.beans;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author alexander.basa
 *
 */
@XmlRootElement
public class Account extends IdValueBean {
	private String registrationId;
	private String firstName;
	private String lastName;
	private String publicKey;
	private String auth;

	/**
	 * @return the registrationId
	 */
	public String getRegistrationId() {
		return this.registrationId;
	}
	/**
	 * @param registrationId the registrationId to set
	 */
	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return this.firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return this.lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the publicKey
	 */
	public String getPublicKey() {
		return this.publicKey;
	}
	/**
	 * @param publicKey the publicKey to set
	 */
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
	/**
	 * @return the auth
	 */
	public String getAuth() {
		return this.auth;
	}
	/**
	 * @param auth the auth to set
	 */
	public void setAuth(String auth) {
		this.auth = auth;
	}
}
