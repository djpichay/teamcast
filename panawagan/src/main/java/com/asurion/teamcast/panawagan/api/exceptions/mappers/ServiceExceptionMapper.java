/**
 * 
 */
package com.asurion.teamcast.panawagan.api.exceptions.mappers;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.asurion.teamcast.panawagan.api.exceptions.ServiceException;

/**
 * @author alexander.basa
 *
 */
@Provider
public class ServiceExceptionMapper implements ExceptionMapper<ServiceException> {

	@Override
	public Response toResponse(ServiceException ex) {
		ex.printStackTrace();
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
	}
}
