/**
 * 
 */
package com.asurion.teamcast.panawagan.api.announcements.image;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.asurion.teamcast.panawagan.api.beans.AnnouncementImage;
import com.asurion.teamcast.panawagan.api.beans.IdValueBean;

/**
 * @author alexander.basa
 *
 */
@Path("images")
public class ImageController {
	@Inject
	private ImageService service;

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public IdValueBean create (
			MultipartFormDataInput input
	) {

		IdValueBean idValue = new IdValueBean();

		Map<String, List<InputPart>> formParts = input.getFormDataMap();

		List<InputPart> inPart = formParts.get("file");

		for (InputPart inputPart : inPart) {

			try {

				// Handle the body of that part with an InputStream

				InputStream istream = inputPart.getBody(InputStream.class, null);
				BufferedImage image = ImageIO.read(istream);
				
				AnnouncementImage announcementImage = new AnnouncementImage();
				announcementImage.setImage(image);
				
				idValue.setId(String.valueOf(this.service.create(announcementImage)));

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return idValue;

	}
	
	@Path("{id}")
	@GET
	@Produces({"image/png", "image/jpeg", "image/gif"})
	public Response retrieve(@PathParam("id") long id) throws IOException {
		AnnouncementImage announcementImage = this.service.retrieve(id);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    ImageIO.write(announcementImage.getImage(), "jpg", baos);
	    byte[] imageData = baos.toByteArray();
	    return Response.ok(imageData).build();
	    
	}

}
