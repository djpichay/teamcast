/**
 * 
 */
package com.asurion.teamcast.panawagan.api.announcements;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;

import com.asurion.teamcast.panawagan.api.BaseService;
import com.asurion.teamcast.panawagan.api.beans.Account;
import com.asurion.teamcast.panawagan.api.beans.Announcement;
import com.asurion.teamcast.panawagan.api.dao.AccountDao;
import com.asurion.teamcast.panawagan.api.dao.AnnouncementDao;
import com.asurion.teamcast.panawagan.api.utils.NotFoundChecker;
import com.asurion.teamcast.panawagan.api.utils.NullAwareBeanUtils;

/**
 * @author alexander.basa
 *
 */
public class AnnouncementService implements BaseService<Announcement> {
	@Inject
	private AnnouncementDao dao;
	
	@Inject
	private AccountDao accountDao;

	@Override
	public long create(Announcement resource) {
		resource.setCreateTime(Calendar.getInstance().getTimeInMillis());
		return this.dao.create(resource);
	}

	@Override
	public Announcement retrieve(long id) {
		Announcement account = this.dao.retreieve(id);
		NotFoundChecker.check(account);
		return account;
	}

	@Override
	public boolean update(long id, Announcement resource) {
		Announcement account = this.dao.retreieve(id);
		NotFoundChecker.check(account);
		resource.setId(null); //make sure that we do not change the id
		NullAwareBeanUtils.copyProperties(account, resource);
		return this.dao.update(account);
	}

	@Override
	public boolean delete(long id) {
		NotFoundChecker.check(this.dao, id);
		return this.dao.delete(id);
	}

	@Override
	public List<Announcement> search(MultivaluedMap<String, String> query) {
		return this.dao.search(query);
	}
	
	public List<Account> listAccounts() {
		return this.accountDao.search(null);
	}
}
