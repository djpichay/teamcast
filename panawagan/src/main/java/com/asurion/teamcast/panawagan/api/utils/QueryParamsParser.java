/**
 * 
 */
package com.asurion.teamcast.panawagan.api.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author alexander.basa
 *
 */
public class QueryParamsParser {
	public static Map<String, String> parse(final String query) {
		Map<String, String> params = new HashMap<>();
		String[] pairs = query.split("&");
		for (String pair : pairs) {
			int idx = pair.indexOf("=");
			params.put(pair.substring(0, idx), (pair.substring(idx + 1)));
		}
		return params;
	}
}