/**
 * 
 */
package com.asurion.teamcast.panawagan.api.announcements.received;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;

/**
 * @author alexander.basa
 *
 */
@ApplicationScoped
public class ReceivedInventory {
	private Map<String,Set<String>> inventory;

	public ReceivedInventory() {
		this.inventory = new HashMap<>();
	}
	
	public void put(final String announcementId, final String accountId) {
		Set<String> accounts = this.inventory.get(announcementId);
		if(null == accounts) {
			accounts = new HashSet<>();
		}
		accounts.add(accountId);
		this.inventory.put(announcementId, accounts);
	}
	
	public Set<String> list(final String announcementId) {
		return this.inventory.get(announcementId);
	}
}