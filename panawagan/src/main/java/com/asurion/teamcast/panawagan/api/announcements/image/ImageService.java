/**
 * 
 */
package com.asurion.teamcast.panawagan.api.announcements.image;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;

import com.asurion.teamcast.panawagan.api.BaseService;
import com.asurion.teamcast.panawagan.api.beans.AnnouncementImage;
import com.asurion.teamcast.panawagan.api.dao.ImageDao;
import com.asurion.teamcast.panawagan.api.utils.NotFoundChecker;
import com.asurion.teamcast.panawagan.api.utils.NullAwareBeanUtils;

/**
 * @author alexander.basa
 *
 */
public class ImageService implements BaseService<AnnouncementImage> {
	@Inject
	private ImageDao dao;

	@Override
	public long create(AnnouncementImage resource) {
		return this.dao.create(resource);
	}

	@Override
	public AnnouncementImage retrieve(long id) {
		AnnouncementImage AnnoucementImage = this.dao.retreieve(id);
		NotFoundChecker.check(AnnoucementImage);
		return AnnoucementImage;
	}

	@Override
	public boolean update(long id, AnnouncementImage resource) {
		AnnouncementImage AnnoucementImage = this.dao.retreieve(id);
		NotFoundChecker.check(AnnoucementImage);
		resource.setId(null); //make sure that we do not change the id
		NullAwareBeanUtils.copyProperties(AnnoucementImage, resource);
		return this.dao.update(AnnoucementImage);
	}

	@Override
	public boolean delete(long id) {
		NotFoundChecker.check(this.dao, id);
		return this.dao.delete(id);
	}

	@Override
	public List<AnnouncementImage> search(MultivaluedMap<String, String> query) {
		return this.dao.search(query);
	}
}
