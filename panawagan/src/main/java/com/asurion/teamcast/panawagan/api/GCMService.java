/**
 * 
 */
package com.asurion.teamcast.panawagan.api;

import java.security.PublicKey;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.asurion.teamcast.panawagan.api.beans.Account;
import com.asurion.teamcast.panawagan.api.beans.Announcement;
import com.asurion.teamcast.panawagan.api.utils.JSONUtil;
import com.google.common.io.BaseEncoding;

import nl.martijndwars.webpush.GcmNotification;
import nl.martijndwars.webpush.Notification;
import nl.martijndwars.webpush.PushService;
import nl.martijndwars.webpush.Utils;

/**
 * @author alexander.basa
 *
 */
public class GCMService {
	// standard url for sending gcm messages
	private static final String gcmUrl = "https://android.googleapis.com/gcm/send/";

	// teamcast is registered under this app id
	private static final String appId = "AIzaSyAHo1hROcWowE1p2wddSuPcqI4ojWGb9DQ";

	public static void send(Announcement announcement, Account user) {
		try {
			Security.addProvider(new BouncyCastleProvider());
			PublicKey publicKey = Utils.loadPublicKey(user.getPublicKey());
			byte[] userAuth = BaseEncoding.base64Url().decode(user.getAuth());
			Notification notification = new GcmNotification(gcmUrl + user.getRegistrationId(), publicKey, userAuth, JSONUtil.toBytes(announcement));
			PushService pushService = new PushService(appId);
			pushService.send(notification);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
