/**
 * 
 */
package com.asurion.teamcast.panawagan.api.announcements.seen;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.asurion.teamcast.panawagan.api.beans.ListBean;

/**
 * @author alexander.basa
 *
 */
@Path("announcements")
public class SeenController {
	@Inject
	private SeenInventory inventory;

	@Path("{announcementId}/seen/{accountId}")
	@PUT
	public void create(@PathParam("announcementId") final String announcementId, @PathParam("accountId") final String accountId) {
		this.inventory.put(announcementId, accountId);
	}

	@Path("{announcementId}/seen")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ListBean list(@PathParam("announcementId") final String announcementId) {
		ListBean value = new ListBean();
		value.setList(this.inventory.list(announcementId));
		return value;
	}
}