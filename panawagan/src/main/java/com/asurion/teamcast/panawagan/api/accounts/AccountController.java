/**
 * 
 */
package com.asurion.teamcast.panawagan.api.accounts;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import com.asurion.teamcast.panawagan.api.BaseController;
import com.asurion.teamcast.panawagan.api.beans.Account;
import com.asurion.teamcast.panawagan.api.beans.IdValueBean;

/**
 * @author alexander.basa
 *
 */
@Path("accounts")
public class AccountController implements BaseController<Account>{
	@Inject
	private AccountService service;

	@Override
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public IdValueBean create(final Account resource) {
		IdValueBean idValue = new IdValueBean();
		long accountId = this.service.create(resource);
		idValue.setId(String.valueOf(accountId));
		return idValue;
	}
	
	@Override
	@Path("{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Account retrieve(@PathParam("id") long id) {
		return this.service.retrieve(id);
	}
	
	@Override
	@Path("{id}")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public void update(@PathParam("id") long id, final Account resource) {
		this.service.update(id, resource);
	}
	
	@Override
	@Path("{id}")
	@DELETE
	public void delete(@PathParam("id") long id) {
		this.service.delete(id);
	}

	@Override
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Account> search(@Context UriInfo uriInfo) {
		MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters(); 
		return this.service.search(queryParams);
	}
}
