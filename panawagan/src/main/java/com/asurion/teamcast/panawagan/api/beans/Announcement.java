/**
 * 
 */
package com.asurion.teamcast.panawagan.api.beans;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author alexander.basa
 *
 */
@XmlRootElement
public class Announcement extends IdValueBean {
	private String title;
	private String message;
	private String content;
	private String imgId;
	private List<String> options;
	private long createTime;

	/**
	 * @return the title
	 */
	public String getTitle() {
		return this.title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return this.message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * @return the url
	 */
	public String getImgId() {
		return imgId;
	}
	/**
	 * 
	 * @param url the url to set
	 */
	public void setImgUrl(String imgId) {
		this.imgId = imgId;
	}
	/**
	 * @return the options
	 */
	public List<String> getOptions() {
		return this.options;
	}
	/**
	 * @param options the options to set
	 */
	public void setOptions(List<String> options) {
		this.options = options;
	}
	/**
	 * @return the createTime
	 */
	public long getCreateTime() {
		return this.createTime;
	}
	/**
	 * @param createTime the createTime to set
	 */
	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}
}
