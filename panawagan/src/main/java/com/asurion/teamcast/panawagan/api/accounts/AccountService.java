/**
 * 
 */
package com.asurion.teamcast.panawagan.api.accounts;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;

import com.asurion.teamcast.panawagan.api.BaseService;
import com.asurion.teamcast.panawagan.api.beans.Account;
import com.asurion.teamcast.panawagan.api.dao.AccountDao;
import com.asurion.teamcast.panawagan.api.utils.NotFoundChecker;
import com.asurion.teamcast.panawagan.api.utils.NullAwareBeanUtils;

/**
 * @author alexander.basa
 *
 */
public class AccountService implements BaseService<Account> {
	@Inject
	private AccountDao dao;

	@Override
	public long create(Account resource) {
		return this.dao.create(resource);
	}

	@Override
	public Account retrieve(long id) {
		Account account = this.dao.retreieve(id);
		NotFoundChecker.check(account);
		return account;
	}

	@Override
	public boolean update(long id, Account resource) {
		Account account = this.dao.retreieve(id);
		NotFoundChecker.check(account);
		resource.setId(null); //make sure that we do not change the id
		NullAwareBeanUtils.copyProperties(account, resource);
		return this.dao.update(account);
	}

	@Override
	public boolean delete(long id) {
		NotFoundChecker.check(this.dao, id);
		return this.dao.delete(id);
	}

	@Override
	public List<Account> search(MultivaluedMap<String, String> query) {
		return this.dao.search(query);
	}
}
