/**
 * 
 */
package com.asurion.teamcast.panawagan.api.beans;

import java.awt.image.BufferedImage;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author alexander.basa
 *
 */
@XmlRootElement
public class AnnouncementImage extends IdValueBean {
	private BufferedImage image;

	/**
	 * @return the image
	 */
	public BufferedImage getImage() {
		return this.image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(BufferedImage image) {
		this.image = image;
	}
}