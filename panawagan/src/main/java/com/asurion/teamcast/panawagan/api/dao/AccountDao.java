/**
 * 
 */
package com.asurion.teamcast.panawagan.api.dao;

import javax.enterprise.context.ApplicationScoped;

import com.asurion.teamcast.panawagan.api.beans.Account;

/**
 * @author alexander.basa
 *
 */
@ApplicationScoped
public class AccountDao extends GenericDao<Account> {
	public AccountDao() {
		super();
	}
}