/**
 * 
 */
package com.asurion.teamcast.panawagan.api.beans;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author alexander.basa
 *
 */
@XmlRootElement
public class IdValueBean {

	private String id;

	/**
	 * @return the id
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
}
