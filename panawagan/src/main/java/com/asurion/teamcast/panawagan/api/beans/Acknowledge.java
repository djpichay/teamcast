/**
 * 
 */
package com.asurion.teamcast.panawagan.api.beans;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author alexander.basa
 *
 */
@XmlRootElement
public class Acknowledge {
	private String option;

	/**
	 * @return the option
	 */
	public String getOption() {
		return this.option;
	}

	/**
	 * @param option the option to set
	 */
	public void setOption(String option) {
		this.option = option;
	}
}