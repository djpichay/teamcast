/**
 * 
 */
package com.asurion.teamcast.panawagan.api;

import java.util.List;

import javax.ws.rs.core.UriInfo;

import com.asurion.teamcast.panawagan.api.beans.IdValueBean;

/**
 * @author alexander.basa
 *
 */
public interface BaseController<R> {
	
	public IdValueBean create(final R resource);
	public R retrieve(long id);
	public void update(long id, final R resource);
	public void delete(long id);
	public List<R> search(UriInfo uriInfo);
}
