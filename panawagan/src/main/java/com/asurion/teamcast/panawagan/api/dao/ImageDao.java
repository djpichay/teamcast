/**
 * 
 */
package com.asurion.teamcast.panawagan.api.dao;

import javax.enterprise.context.ApplicationScoped;

import com.asurion.teamcast.panawagan.api.beans.AnnouncementImage;

/**
 * @author alexander.basa
 *
 */
@ApplicationScoped
public class ImageDao extends GenericDao<AnnouncementImage> {
	public ImageDao() {
		super();
	}
}