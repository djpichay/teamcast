/**
 * 
 */
package com.asurion.teamcast.panawagan.api.exceptions;

/**
 * @author alexander.basa
 *
 */
public class NotFoundException extends RuntimeException {
	private static final long serialVersionUID = 9022043237001452890L;

	public NotFoundException() {
		super();
	}

	/**
	 * @param message
	 */
	public NotFoundException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public NotFoundException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public NotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public NotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
