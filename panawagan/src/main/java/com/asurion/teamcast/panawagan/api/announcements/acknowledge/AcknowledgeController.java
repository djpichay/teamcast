/**
 * 
 */
package com.asurion.teamcast.panawagan.api.announcements.acknowledge;

import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.asurion.teamcast.panawagan.api.beans.Acknowledge;

/**
 * @author alexander.basa
 *
 */
@Path("announcements")
public class AcknowledgeController {
	@Inject
	private AcknowledgeInventory inventory;

	@Path("{announcementId}/acknowledge/{accountId}")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public void create(@PathParam("announcementId") final String announcementId, @PathParam("accountId") final String accountId, final Acknowledge acknowledge) {
		this.inventory.put(announcementId, accountId, acknowledge.getOption());
	}

	@Path("{announcementId}/acknowledge")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Set<String>> list(@PathParam("announcementId") final String announcementId) {
		return this.inventory.list(announcementId);
	}
}