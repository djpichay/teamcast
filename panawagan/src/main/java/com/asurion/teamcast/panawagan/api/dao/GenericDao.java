/**
 * 
 */
package com.asurion.teamcast.panawagan.api.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MultivaluedMap;

import com.asurion.teamcast.panawagan.api.beans.IdValueBean;

/**
 * @author alexander.basa
 *
 */
public class GenericDao<R extends IdValueBean> {
	private long ctr;
	private Map<String,R> db;
	
	public GenericDao() {
		this.ctr = 0;
		this.db = new HashMap<>();
	}

	public long create(R resource) {
		synchronized(this.db) {
			++this.ctr;
			String accountId = String.valueOf(this.ctr);
			resource.setId(accountId);
			this.db.put(accountId, resource);
			return this.ctr;
		}
	}

	public R retreieve(long id) {
		String key = String.valueOf(id);
		return this.db.get(key);
	}

	public boolean update(R resource) {
		R account = this.db.get(resource.getId());
		if(null == account) {
			return false;
		}
		return this.db.put(resource.getId(), resource) != null;
	}

	public boolean delete(long id) {
		String accountId = String.valueOf(id);
		R account = this.db.remove(accountId);
		if(null == account) {
			return false;
		}	
		return true;
	}

	public List<R> search(MultivaluedMap<String, String> query) {
		List<R> list = new ArrayList<>();
		Collection<R> c = this.db.values();
		list.addAll(c);
		return list;
	}
}
