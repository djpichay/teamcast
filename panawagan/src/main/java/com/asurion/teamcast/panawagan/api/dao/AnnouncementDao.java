/**
 * 
 */
package com.asurion.teamcast.panawagan.api.dao;

import javax.enterprise.context.ApplicationScoped;

import com.asurion.teamcast.panawagan.api.beans.Announcement;

/**
 * @author alexander.basa
 *
 */
@ApplicationScoped
public class AnnouncementDao extends GenericDao<Announcement> {
	public AnnouncementDao() {
		super();
	}
}