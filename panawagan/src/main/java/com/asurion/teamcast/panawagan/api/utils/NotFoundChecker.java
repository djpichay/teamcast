/**
 * 
 */
package com.asurion.teamcast.panawagan.api.utils;

import com.asurion.teamcast.panawagan.api.beans.IdValueBean;
import com.asurion.teamcast.panawagan.api.dao.GenericDao;
import com.asurion.teamcast.panawagan.api.exceptions.NotFoundException;

/**
 * @author alexander.basa
 *
 */
public class NotFoundChecker {
	

	public static <T extends IdValueBean> void check(GenericDao<T> dao, long id) {
		check(dao.retreieve(id));
	}
	
	public static <T extends IdValueBean> void check(T resource) {
		if(null == resource) {
			throw new NotFoundException();
		}
	}
}