/**
 * 
 */
package com.asurion.teamcast.panawagan.api.utils;

import com.asurion.teamcast.panawagan.api.beans.Account;

/**
 * @author alexander.basa
 *
 */
public class NullAwareBeanUtilsTest {

	public static void main(String args[]) {
		Account source = new Account();
		Account destination = new Account();
		
		source.setAuth("auth");
		source.setFirstName("alex");
		
		NullAwareBeanUtils.copyProperties(destination, source);
		System.out.println("finished");
	}

}
